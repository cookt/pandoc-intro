---
title: Pandoc-Intro
author: Tamara Cook, M.Sc.
date: 11.08.2021
institute: Philipps-Universität Marburg
---

# Intro und Setup

## Was ist Pandoc?

1. Ein universeller Dokumentenkonvertierer
   - zwischen Markup-Formaten wie Markdown oder Mediawiki
   - Aus Markup-Formaten in visuell ansprechende Dokumente (HTML, Word, E-Books, Präsentationen)
2. Ein erweiterter Markdown-Dialekt, der z.B. Gleitumgebungen und Zitate beherrscht

## Warum Markdown und Pandoc?

- Trennung von Inhalt und Darstellung in Dokumenten
- Darstellung unabhängig vom Inhalt anpassbar
- Verschiedene Ausgabedokumente mit einer Quelle erzeugen
- Pandoc läuft lokal installiert, aber auch als [Continuous Deployment]

## Setup

- Bitte Pandoc gemäß [Installationsanleitung] downloaden und installieren
- Empfehlung: Windows Terminal von Microsoft aus dem Windows Store installieren für eine angenehmere Kommandozeile
- Bitte [Visual Studio Code][vscode] installieren

# Workflow

# Pandoc Markdown

# Darstellung anpassen

[continuous deployment]: https://github.com/pandoc/pandoc-action-example
[installationsanleitung]: https://pandoc.org/installing.html
[vscode]: https://code.visualstudio.com

<!-- markdownlint-disable-file MD025 -->
